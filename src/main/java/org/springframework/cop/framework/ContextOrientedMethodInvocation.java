package org.springframework.cop.framework;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.support.AopUtils;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Method;

public class ContextOrientedMethodInvocation implements MethodInvocation {

    protected final Log logger = LogFactory.getLog(ContextOrientedMethodInvocation.class);

    private final Object target;

    private final Method method;

    private final Object[] arguments;

    public ContextOrientedMethodInvocation(Object target, Method method, Object[] arguments) {
        this.target = target;
        this.method = method;
        this.arguments = arguments;
    }

    @Override
    public Method getMethod() {
        return this.method;
    }

    @Override
    public Object[] getArguments() {
        return this.arguments;
    }

    @Override
    public Object proceed() throws Throwable {
        return AopUtils.invokeJoinpointUsingReflection(this.target, this.method, this.arguments);
    }

    @Override
    public Object getThis() {
        return this.target;
    }

    @Override
    public AccessibleObject getStaticPart() {
        return this.method;
    }


}
