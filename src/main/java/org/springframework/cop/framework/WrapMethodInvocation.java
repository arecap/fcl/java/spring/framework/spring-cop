package org.springframework.cop.framework;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.ProxyMethodInvocation;
import org.springframework.util.Assert;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Method;
import java.util.LinkedHashSet;
import java.util.Set;

public class WrapMethodInvocation implements MethodInvocation {

    private final Set<MethodInvocation> proceedMethodInvocationChain = new LinkedHashSet<>();

    private final MethodInvocation jointPoint;

    public WrapMethodInvocation(MethodInvocation jointPoint) {
        this.jointPoint = jointPoint;
    }

    public void add(MethodInvocation proceedMethodInvocation) {
        proceedMethodInvocationChain.add(proceedMethodInvocation);
    }

    public void addAll(Set<MethodInvocation> proceedMethodInvocations) {
        proceedMethodInvocationChain.addAll(proceedMethodInvocations);
    }

    public void setArguments(Object... arguments) {
        Assert.isAssignable(ProxyMethodInvocation.class, jointPoint.getClass());
        ((ProxyMethodInvocation) jointPoint).setArguments(arguments);
    }

    @Override
    public Method getMethod() {
        return this.jointPoint.getMethod();
    }

    @Override
    public Object[] getArguments() {
        return this.jointPoint.getArguments();
    }

    @Override
    public Object proceed() throws Throwable {
        MethodInvocation methodInvocation = proceedMethodInvocationChain.iterator().next();
        proceedMethodInvocationChain.remove(methodInvocation);
        if(proceedMethodInvocationChain.isEmpty()) {
            return methodInvocation.proceed();
        }
        return
                new ContextOrientedMethodInvocation(methodInvocation.getThis(),
                        methodInvocation.getMethod(),
                        new Object[]{this}).proceed();
    }

    @Override
    public Object getThis() {
        return this.jointPoint.getThis();
    }

    @Override
    public AccessibleObject getStaticPart() {
        return this.jointPoint.getStaticPart();
    }

}

