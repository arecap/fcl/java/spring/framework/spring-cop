package org.springframework.cop.framework;

import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Method;
import java.util.Set;

public class TrackMethodInvocation implements MethodInvocation {

    private final MethodInvocation beanMethodInvocation;

    private final Set<MethodInvocation> proceedJoinPoints;

    public TrackMethodInvocation(MethodInvocation beanMethodInvocation, Set<MethodInvocation> proceedJoinPoints) {

        this.beanMethodInvocation = beanMethodInvocation;
        this.proceedJoinPoints = proceedJoinPoints;
    }


    @Override
    public Method getMethod() {
        return beanMethodInvocation.getMethod();
    }

    @Override
    public Object[] getArguments() {
        return beanMethodInvocation.getArguments();
    }

    @Override
    public Object proceed() throws Throwable {
        Object returnMulticast = beanMethodInvocation.proceed();
        for(MethodInvocation contextOrientedMethodInvocation: proceedJoinPoints) {
            Object returnTrack = contextOrientedMethodInvocation.proceed();
            if(returnTrack != null && returnTrack.getClass().isAssignableFrom(returnMulticast.getClass()))
                returnMulticast = returnTrack;
        }
        return returnMulticast;
    }

    @Override
    public Object getThis() {
        return beanMethodInvocation.getThis();
    }

    @Override
    public AccessibleObject getStaticPart() {
        return beanMethodInvocation.getStaticPart();
    }
}

