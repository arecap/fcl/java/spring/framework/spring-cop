/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.cop.support.annotation;

import org.contextualj.lang.annotation.expression.LexicalExpression;
import org.contextualj.lang.weaving.PointcutLinkSpecification;
import org.springframework.cop.annotation.ExpressionResolver;
import org.springframework.cop.support.ExpressionValueResolver;
import org.springframework.core.annotation.AnnotatedElementUtils;

import java.lang.reflect.AnnotatedElement;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
@ExpressionResolver
public class LexicalExpressionLink implements
        PointcutLinkSpecification<LexicalExpression>, ExpressionValueResolver, SpringLexicalExpressionResolver {


    @Override
    public boolean isInverseMultiplexerLink(AnnotatedElement signal, AnnotatedElement link) {
        LexicalExpression lexicalExpression = evaluate(link);
        return lexicalExpression == null ? false :
                resolveLexicalExpression(lexicalExpression.lex(),
                        evaluate(lexicalExpression.value()), evaluate(lexicalExpression.valid()));
    }

    @Override
    public LexicalExpression evaluate(AnnotatedElement link) {
        return AnnotatedElementUtils.getMergedAnnotation(link, LexicalExpression.class);
    }
}
